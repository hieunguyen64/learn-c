#include <stdio.h>

void main()
{
	int a;
	char b[] = "Class : ";
	char c[50];
		
	printf("Input class number : ");
	scanf("%d", &a);
	
	printf("\n");
	
	if (a == 1)
		strcpy(c, "Freshman");
	if (a == 2)
		strcpy(c, "Sophomore");
	if (a == 3)
		strcpy(c, "Junior");
	if (a == 4)
		strcpy(c, "Senior");
	if (a == 5)
		strcpy(c, "Graduate");
	
	if (c[0] == '\0')
		printf("Unknown class");
	else {
		printf("%s", b);
		printf("%s", c);
	}
	
	printf("\n");
	
	int x, y, z, d;
	
	printf("\nInput literature mark : ");
	scanf("%d", &x);
	printf("Input history mark : ");
	scanf("%d", &y);
	printf("Input geography mark : ");
	scanf("%d", &z);
	
	d = (x + y + z) / 3;
	
	if (d >= 90)
		printf("Grade A");
	else if (d >= 80 && d < 90)
		printf("Grade B");
	else if (d >= 70 && d < 80)
		printf("Grade C");
	else if (d >= 60 && d < 70)
		printf("Grade D");

	else
		printf("Grade F");
		
	printf("\n");
		
	if (d >=70 )
		printf("\nYou passed");
	else
		printf("\nYou failed");
}












