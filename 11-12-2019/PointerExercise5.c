#include <stdio.h>
#include <malloc.h>

void main()
{
	int *ptr, number, i;
	printf("Nhap so luong n : ");
	scanf("%d", &number);
	ptr = (int *) malloc(number*sizeof(int));
	if (ptr != NULL)
	{
		for(i = 0; i < number; i++)
		{
			*(ptr+i) = i;
		}
		for(i = number; i > 0; i--)
		{
			printf("%d", *(ptr + (i - 1)));
		}
		free(ptr);
		return 0;
	}
	else
	{
		printf("Khong du bo nho : ");
		return 1;
	}
}


