#include <stdio.h>
#include <conio.h>
#include <math.h>

//Viet ham nhap xuat mang 2 chieu cac so nguyen

/*void NhapMaTran (int a[][100], int m, int n)
{
	for(int i = 0; i < m; i++)
		for(int j = 0; j < n; j++)
		{
			printf("a[%d][%d] = ", i, j);
			scanf("%d", &a[i][j]);
		}
}

void XuatMaTran (int a[][100], int m, int n)
{
	for(int i = 0; i < m; i++)
	{
		for(int j = 0; j < n; j++)
		
			printf("%d\t", a[i][j]);
			printf("\n");
	}
}

int main()
{
	int a[100][100];
	int m, n;
	printf("Insert number of rows : ");
	scanf("%d", &n);
	printf("Insert number of columns : ");
	scanf("%d", &m);
	printf("Insert matrix : \n");
	NhapMaTran (a, m, n);
	XuatMaTran (a, m, n);
}
*/

//Viet ham tinh tong cac so duong trong ma tran
void NhapMaTran (float a[][100], int m, int n)
{
	for (int i = 0; i < m; i++)
		for(int j = 0; j < n; j++)
		{
			printf("a[%d[%d] = ", i, j);
			scanf("%f", &a[i][j]);
		}
}

void XuatMaTran (float a[][100], int m, int n)
{
	for (int i = 0; i < m; i++)
	{
			for(int j = 0; j < n; j++)
			printf("%.2f\t", a[i][j]);
			printf("\n");
	}
}

float TinhTongCacSoDuong (float a[][100], int m, int n)
{
	int i,j;
	float sum = 0.0;
		for (int i = 0; i < m; i++)
	{
			for(int j = 0; j < n; j++)
			if (a[i][j] > 0)
			{
				sum += a[i][j];
			}
	}
	return sum;
}

int main()
{
	int m,n;
	float a[100][100];
	printf("Insert number of rows : ");
	scanf("%d", &n);
	printf("Insert number of columns : ");
	scanf("%d", &m);
	printf("Insert Matrix");
	NhapMaTran (a,m,n);
	XuatMaTran (a,m,n);
	TinhTongCacSoDuong(a,m,n);
	printf("\nSum all positive elements = %.2f", TinhTongCacSoDuong(a,m,n));
}
